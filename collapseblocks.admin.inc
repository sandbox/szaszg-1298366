<?php

/**
 * @file
 * Administrative page callbacks for the collapseblocks module.
 */

/**
 * Administration settings form.
 */
//function collapseblocks_settings_form($form, &$form_state, $form_id = 'main') {
//
//  return collapseblocks_build_main_settings('collapseblocks_main_settings', $settings);
//}

//function collapseblocks_build_main_settings($id, &$settings) {
function collapseblocks_settings_form($form, &$form_state, $form_id = 'main') {
  global $base_url;
  global $collapseblocks_settings;

  _collapseblocks_settings_load();

  $p = $base_url . '/' . drupal_get_path('module', 'collapseblocks');

  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings'),
  );

  $form['default']['collapseblocks_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Make blocks collapsible.' ),
    '#options' => array('0' => t('Disabled'), '1' => t('Enabled - expanded'), '2' => t('Enabled - collapsed')),
    '#default_value' => $collapseblocks_settings['collapseblocks']['enable'],
  );

  $form['icons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Icon settings'),
    '#description' => t('If toggled on, the following settings will be displayed.'),
  );

  $form['icons']['collapseblocks_default_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default icon set (<img class="collapseblocks-config" src="'. $p . '/icons/block-collapse.png'.'">;<img class="collapseblocks-config" src="'. $p . '/icons/block-expand.png'.'">)'),
    '#description' => t('Check here to use <i>Collapse blocks</i> default icons for <i>collapse</i> and <i>expand</i>.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['default_icons'],
  );
  $form['icons']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the icon settings when using the default.
      'invisible' => array(
        'input[name="collapseblocks_default_icons"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['icons']['settings']['collapseblocks_collapse_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Set icon file for collapse.'),
    '#description' => t('Use &lt;default&gt; for default icon.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['collapse_icon'],
  );
  $form['icons']['settings']['collapseblocks_expand_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Set icon file for expand.'),
    '#description' => t('Use &lt;default&gt; for default icon.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['expand_icon'],
  );
  $form['icons']['collapseblocks_icon_pos'] = array(
    '#type' => 'select',
    '#title' => t('Icon position'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['icon_pos'],
    '#options' => array('auto' => 'auto', 'left' => t('left'), 'right' => t('right')),
  );

  $form['animation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Animation settings'),
//    '#description' => t('If toggled on, the following settings will be displayed.'),
  );
  $form['animation']['collapseblocks_slide_type'] = array(
    '#type' => 'select',
    '#title' => t('Default animation type'),
    '#options' => array(0 => t('Nothing'), 1 => t('Slide'), 2 => t('Fade and slide')),
    '#description' => t('Slide is the Drupal default while Fade and slide adds a nice fade effect.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['slide_type'],
  );
  $form['animation']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the animation settings when using no animation.
      'invisible' => array(
        'select[name="collapseblocks_slide_type"]' => array('value' => '0'),
      ),
    ),
  );

  $form['animation']['settings']['collapseblocks_slide_speed'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed'),
    '#options' => drupal_map_assoc(array('50', '250', '500', '750', '1000', '1250', '1500', '2000')),
    '#description' => t('The animation speed in milliseconds.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['slide_speed'],
  );
  $form['animation']['settings']['collapseblocks_slide_ease'] = array(
    '#type' => 'select',
    '#title' => t('Animation easing effect'),
    '#options' => drupal_map_assoc(array('linear', 'swing')),
    '#description' => t('The animation easing effect.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['slide_ease'],
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['collapseblocks_cookie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always use cookies'),
    '#description' => t('Save blocks\' state to cookies instead of server\'s <i>DB</i> for authenticated users too.'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['cookie'],
  );
  $form['advanced']['collapseblocks_empty_subject'] = array(
    '#type' => 'checkbox',
    '#title' => t('Makes blocks with empty subject collapsible.'),
    '#description' => t('You probably shouldn\'t...'),
    '#default_value' => $collapseblocks_settings['collapseblocks']['empty_subject'],
  );

  $form['collapseblocks_item'] = array(
    '#type' => 'item',
    '#markup' => t('You can change block settings on the <a href="!url">block administration page</a>', array('!url' => url('admin/structure/block'))),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('collapseblocks_adminform_submit')
  );
  return $form;
}

function collapseblocks_adminform_submit($form, &$form_state) {
  foreach ($form_state['values'] as $k => $v ) {
    if (substr($k, 0, 15) == 'collapseblocks_') $data[substr($k, 15)] = $v;
  }
  _collapseblocks_settings_save('collapseblocks', $data);
};
