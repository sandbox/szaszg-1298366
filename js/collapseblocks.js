//toggle visibility

(function($) {

Drupal.behaviors.collapseblocks = {

  attach: function(context, settings) {
    $('body').once('collapseblocks', function () {
      $.each(Drupal.settings.collapseblocks['ids'], function(i, elementID) {
        var contentID;
        var parent = $('#' + elementID + '-H').parent();
        var uid = Drupal.settings.collapseblocks['uid'];
        var block_hidden = Drupal.settings.collapseblocks.block[elementID];
        var icon_pos = Drupal.settings.collapseblocks['icon_pos'];

        if( icon_pos == 'auto' ) icon_pos = document.dir == 'ltr' ? 'right' : 'left';

        $('<img />', {'id': elementID,
                      'class': 'collapseblocks-icon',
                      'src': Drupal.settings.collapseblocks['placeholder_icon']}).appendTo($('#' + elementID + '-S').parent());

        if( icon_pos == 'left' ) $('#' + elementID).addClass('collapseblocks-icon-left');

        $('#' + elementID + '-H').remove();	//remove dummy html tag
        $('#' + elementID + '-S').remove();	//remove dummy html tag

        if( $(parent).attr('id') === undefined || $(parent).attr('id').length == 0 )
          $(parent).attr('id', elementID + '-H');
        contentID = $(parent).attr('id');
        Drupal.settings.collapseblocks.hideid[elementID] = contentID;

        if( Drupal.settings.collapseblocks['cookie'] ) {
          block_hidden = $.cookie(elementID + '--uid-' + uid );
        }
        if( block_hidden ) {
          $('#' + elementID).addClass('collapseblocks-collapsed');
          $('#' + contentID).toggle();
//          $('#' + contentID).addClass('collapseblocks-collapsed');
        }

//add function to element
      $('#' + elementID).click(function() {
        var uid = Drupal.settings.collapseblocks['uid'];
        var cookieid = Drupal.settings.collapseblocks['cookie'] ? $(this).attr('id') + '--uid-' + uid : 0;
        var prop = {height: 'toggle'};
        var save_arg;
        contentID = Drupal.settings.collapseblocks.hideid[$(this).attr('id')];

        if( !$('#' + contentID).hasClass('collapseblocks-clicked') ) {
          $('#' + contentID).addClass('collapseblocks-clicked');
          if( $('#' + contentID).is(':hidden') == false ) {
            if( save_arg !== 0 ) {
              if( cookieid ) $.cookie(cookieid, null);
              else save_arg = {id: $(this).attr('id'), val: 0};
            }
            $(this).addClass('collapseblocks-collapsed');
          } else {
            if( save_arg !== 0 ) {
              if( cookieid ) $.cookie(cookieid, 1, { path: '/', expires: 0 });
              else save_arg = {id: $(this).attr('id'), val: 1};
            }
            $(this).removeClass('collapseblocks-collapsed');
          }

//  save block state to server
          if( save_arg !== 0 ) {
            $.ajax({
              type: 'GET',
              url: '?q=admin/config/user-interface/collapseblocks/ajax-save',
              data: save_arg,
              dataType: 'json'
            });
          }

          if( Drupal.settings.collapseblocks['slide_type'] == 0 ) {
            $('#' + contentID).toggle();
            $('#' + contentID).removeClass('collapseblocks-clicked');
          }
          else {
            if( Drupal.settings.collapseblocks['slide_type'] == 2 ) {
              prop.opacity = 'toggle';
            }
            $('#' + contentID).animate(prop, parseInt(Drupal.settings.collapseblocks['slide_speed']),
                                           Drupal.settings.collapseblocks['slide_ease'],
                                           function() {$(this).removeClass('collapseblocks-clicked');} );
          }
        }
      });
    });
    });
  }
};

})(jQuery);