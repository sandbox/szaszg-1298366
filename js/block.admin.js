// $Id$
(function ($) {

/**
 * Provide the summary information for the block settings vertical tabs.
 */
Drupal.behaviors.collapseblocksSettingsSummary = {
  attach: function (context) {
    // The drupalSetSummary method required for this behavior is not available
    // on the Blocks administration page, so we need to make sure this
    // behavior is processed only if drupalSetSummary is defined.
    if (typeof jQuery.fn.drupalSetSummary == 'undefined') {
      return;
    }
    $('fieldset#edit-collapseblocks', context).drupalSetSummary(function (context) {

      var $check = $('input[name="collapseblocks_enable"]:checked', context);
      if ($check.val() == 1) {
        return Drupal.t('Yes, expanded');
      }
      else if ($check.val() == 2) {
        return Drupal.t('Yes, collapsed');
      }
      else if ($check.val() == -1) {
        return Drupal.t('Default');
      }
      else {
        return Drupal.t('No');
      }
    });
  }
};
})(jQuery);
