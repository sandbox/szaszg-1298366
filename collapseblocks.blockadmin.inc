<?php

/**
 * @file
 * Administrative page callbacks for the collapseblocks module.
 */

/**
 * Administration settings form.
 */
function collapseblocks_blocksettings_form($form, &$form_state) {
  global $base_url;
  global $collapseblocks_settings;

  _collapseblocks_settings_load();

  $blocks = _block_rehash();
  $markup = '<table>';
  $def_enable = array(t('Disabled'), t('Enabled - expanded'), t('Enabled - collapsed'));
  $def_enable = $def_enable[0 + $collapseblocks_settings['collapseblocks']['enable']];
  $enable_a = array( '-1' => t('Default (%def)', array('%def' => $def_enable)), '0' => t('Disabled'), '1' => t('Enabled - expanded'), '2' => t('Enabled - collapsed'));

  foreach ($blocks as $i => $block) {
    $myid = $block['module'] . '_' . $block['delta'];
    $enable = array_key_exists($myid, $collapseblocks_settings) ? $collapseblocks_settings[$myid]['enable'] : -1;

    $markup .= '<tr>';
    $markup .= '<td title="' . $myid . '"><strong>' . check_plain($block['info']) . '</strong></td>';
    $markup .= '<td>' . t('Collapse') . ': ' . $enable_a[$enable] . '<br>' . '</td>';
    $markup .= '<td>' . l(t('configure'), 'admin/structure/block/manage/' . $block['module'] . '/' . $block['delta'] . '/configure') . '</td>';
  }
  $markup .= '</table>';
  $form['markup'] = array(
        '#type' => 'item',
        '#markup' => $markup,
  );

  $form['#submit'][] = 'collapseblocks_form_submit';
  return $form;
}
