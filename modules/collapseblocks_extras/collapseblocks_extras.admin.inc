<?php

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function collapseblocks_extras_block_settings_form() {
  $blocks = array();
  $result = db_query("SELECT DISTINCT module, delta FROM {block} WHERE status = 1 ORDER BY bid");

  foreach ($result as $block) {
    $blocks[$block->module][$block->delta] = $block->module . '-' . $block->delta;
  }
  foreach ($blocks as $b => $block) {
    $info = module_invoke($b, 'block_info');
    foreach($block as $d => $delta) {
      if (isset($info[$d])) {
        $blocks[$b][$d] = $info[$d]['info'];
      }
    }
  }

  return collapseblocks_build_settings_form($blocks);
}

function collapseblocks_build_settings_form(&$blocks, $uid = NULL) {
/*
  $settings = variable_get($id, array(
                              'collapse_icon' => '<default>',
                              'expand_icon' => '<default>',
                              'slide_type' => 1,
                              'slide_speed' => 250,
                              'slide_ease' => 'linear',
                              'enable_empty_subject' => FALSE,
                              'blocks' => NULL ));
*/
  $settings = array();////

  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocks'),
//    '#collapsible' => TRUE,
//    '#collapsed' => TRUE,
    '#description' => t('Configure block settings'),
  );

  $form['blocks']['tabs'] = array(
    '#type' => 'vertical_tabs',
#    '#attached' => array(
#      'js' => array(drupal_get_path('module', 'block') . '/block.js'),
#    ),
  );
  foreach($blocks as $module => $v) {
    foreach($v as $delta => $info) {
    $myid = $module . '-' . $delta;

    $form['blocks']['tabs'][$myid] = array(
      '#type' => 'fieldset',
      '#title' => $info,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'tabs',
      '#weight' => '10',
    );
    $form['blocks']['tabs'][$myid]['collapseblocks_block_' . $myid] = array(
      '#type' => 'radios',
      '#title' => t('Make \'%info\' block collapsible.<br />Id: %myid', array('%info' => $info, '%myid' => $myid ) ),
      '#options' => array( 0 => t('Disabled'), 1 => t('Enabled - expanded'), 2 => t('Enabled - collapsed'), 3 => t('Enabled - always collapsed') ),
      '#default_value' => variable_get('collapseblocks_block_' . $myid, 1),
    );
  }
  }
/*
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;*/
  return system_settings_form($form);
}

function collapseblocks_settings_theme_form($form_id, $form_status, $theme = 'theme/bartik') {

  $mytheme = substr($theme, 6);
  $blocks = array();
  $result = db_query("SELECT DISTINCT module, delta FROM {block} WHERE theme = '" . $mytheme . "' AND status = 1 ORDER BY bid");
  foreach ($result as $block) {
    $blocks[]=$block->module . '-' . $block->delta;
  }

  $form['collapseblocks_collapse_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Set icon file for collapse. -7- ') . $theme,
    '#description' => $r . '<xmp>' . print_r($blocks, TRUE) . '</xmp>',
    '#default_value' => variable_get('collapseblocks_collapse_icon', '<default>'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

//  return collapseblocks_build_settings_form('collapseblocks_theme_settings_' . $mytheme, $blocks);

  return $form;
//  return system_settings_form($form);
}

/**
 * q = user/x/collapseblocks.
 */
//function collapseblocks_user_page($account) {
function collapseblocks_extras_user_settings_page($form, &$form_state, $account) {
  global $user;
  print "<pre>" . print_r($user, TRUE) . "</pre>";
  $form['collapseblocks_slide_type'] = array(
    '#type' => 'select',
    '#title' => t('Default animation type' . $account->uid),
    '#options' => array(1 => t('Slide'), 2 => t('Fade and slide')),
    '#description' => t('Slide is the Drupal default while Fade and slide adds a nice fade effect.'),
    '#default_value' => variable_get('collapseblocks_slide_type', 1),
  );
  $form['collapseblocks_slide_speed'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed'),
    '#options' => drupal_map_assoc(array('50', '250', '500', '750', '1000', '1250', '1500', '2000')),
    '#description' => t('The animation speed in milliseconds.'),
    '#default_value' => variable_get('collapseblocks_slide_speed', 250),
  );
  $form['collapseblocks_slide_ease'] = array(
    '#type' => 'select',
    '#title' => t('Animation easing effect'),
    '#options' => drupal_map_assoc(array('linear', 'swing')),
    '#description' => t('The animation easing effect.'),
    '#default_value' => variable_get('collapseblocks_slide_ease', 'linear'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

/**
 * Form submission handler for _form().
 *
 */
function collapseblocks_user_settings_page_submit($form, &$form_state) {
  global $user, $language;

  $values = $form_state['values'];
  drupal_set_message(t('Your message has been sent.'));
//  $form_state['redirect'] = user_access('access user profiles') ? 'user/' . $values['recipient']->uid : '';
}

